.PHONY: edit clean data s3/upload s3/download requirements

#################################################################################
# GLOBALS                                                                       #
#################################################################################

PROJECT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
PROJECT_NAME = genetic-expression

AWS_BUCKET = [OPTIONAL] your bucket and folder for s3, as you would use it with the aws cli. Do not include a trailing / in the url. For example s3://foo/bar
# use AWS_DEFAULT_PROFILE to change the profile used
PYTHONPATH = $(CURDIR)
R_LIBS = $(CURDIR)/src/R/r-lib

#################################################################################
# COMMANDS                                                                      #
#################################################################################

## Run Jupyter Lab
edit: .make/requirements-python .make/requirements-R
	PYTHONPATH=$(PYTHONPATH) \
	R_LIBS=$(R_LIBS) \
		poetry run jupyter lab --ip 0.0.0.0

## Delete all compiled Python files
clean:
	find . -type f -name "*.py[co]" -delete
	find . -type d -name "__pycache__" -delete

## Make Dataset
data: .make/requirements-python data/interim/GSE140829.gz.parquet data/interim/GSE18309.gz.parquet data/interim/GSE85426.gz.parquet

data/external/GSE140829_final_normalized_data.txt data/external/GSE140829_datalabels.xlsx data/external/GSE85426_normalized_data_m3_use.txt data/external/GSE85426_datalabels.xlsx data/external/GSE18309_RMAnorm_2.txt data/external/GSE18309_datalabels.xlsx :
	$(error these files come from the processed dataset, place these in the data/external folder)

data/external/GSE85426_series_matrix.txt.gz :
	wget -O $@ https://ftp.ncbi.nlm.nih.gov/geo/series/GSE85nnn/GSE85426/matrix/GSE85426_series_matrix.txt.gz

data/interim/GSE140829.gz.parquet : data/external/GSE140829_final_normalized_data.txt data/external/GSE140829_datalabels.xlsx
	PYTHONPATH=$(PYTHONPATH) \
		poetry run python -m src.data.to_parquet convert-dataset-1 \
			--data-file data/external/GSE140829_final_normalized_data.txt \
			--label-file data/external/GSE140829_datalabels.xlsx \
			--output-file $@

data/interim/GSE85426.gz.parquet : data/external/GSE85426_normalized_data_m3_use.txt data/external/GSE85426_datalabels.xlsx data/external/GSE85426_series_matrix.txt.gz
	PYTHONPATH=$(PYTHONPATH) \
		poetry run python -m src.data.to_parquet convert-dataset-2 \
			--data-file data/external/GSE85426_normalized_data_m3_use.txt \
			--label-file data/external/GSE85426_datalabels.xlsx \
			--matrix-file data/external/GSE85426_series_matrix.txt.gz \
			--output-file $@

data/interim/GSE18309.gz.parquet : data/external/GSE18309_RMAnorm_2.txt data/external/GSE18309_datalabels.xlsx
	PYTHONPATH=$(PYTHONPATH) \
		poetry run python -m src.data.to_parquet convert-dataset-3 \
			--data-file data/external/GSE18309_RMAnorm_2.txt \
			--label-file data/external/GSE18309_datalabels.xlsx \
			--output-file $@

data/interim/PlasmaProteinStudy-5.gz.parquet : data/external/AD_plasma_protein.xlsx data/external/AD_plasma_protein_MCI.xlsx data/external/AD_plasma_protein_test.xlsx data/external/AD_plasma_protein_testOD.xlsx
	PYTHONPATH=$(PYTHONPATH) \
		poetry run python -m src.data.to_parquet convert-dataset-4 \
			--data-file-1 data/external/AD_plasma_protein.xlsx \
			--data-file-2 data/external/AD_plasma_protein_MCI.xlsx \
			--data-file-3 data/external/AD_plasma_protein_test.xlsx \
			--data-file-4 data/external/AD_plasma_protein_testOD.xlsx \
			--output-file $@

## Upload Data to S3
s3/upload:
	aws s3 sync data/ $(AWS_BUCKET)/data/
	aws s3 sync models/ $(AWS_BUCKET)/models/

## Download Data from S3
s3/download:
	aws s3 sync $(AWS_BUCKET)/data/ data/
	aws s3 sync $(AWS_BUCKET)/models/ models/

## Install Python Dependencies
requirements/python: .make/requirements-python

## Install R Dependencies
requirements/R: .make/requirements-R

.make/requirements-python: pyproject.toml poetry.lock
ifneq (,$(wildcard .python-version))
	poetry env use $(shell cat .python-version)
endif
	PYTHON_KEYRING_BACKEND=keyring.backends.null.Keyring poetry install
	if [ ! -e .make ]; then mkdir .make; fi
	touch $@

poetry.lock:
ifneq (,$(wildcard .python-version))
	poetry env use $(shell cat .python-version)
endif
	PYTHON_KEYRING_BACKEND=keyring.backends.null.Keyring poetry install
	if [ ! -e .make ]; then mkdir .make; fi
	touch $@

.make/requirements-R: src/R/prepare.R
	cd src/R ; R_LIBS=r-lib/ Rscript prepare.R

#################################################################################
# PROJECT RULES                                                                 #
#################################################################################



#################################################################################
# Self Documenting Commands                                                     #
#################################################################################

.DEFAULT_GOAL := show-help
# See <https://gist.github.com/klmr/575726c7e05d8780505a> for explanation.
.PHONY: show-help
show-help:
	@echo "$$(tput setaf 2)Available rules:$$(tput sgr0)";sed -ne"/^## /{h;s/.*//;:d" -e"H;n;s/^## /---/;td" -e"s/:.*//;G;s/\\n## /===/;s/\\n//g;p;}" ${MAKEFILE_LIST}|awk -F === -v n=$$(tput cols) -v i=4 -v a="$$(tput setaf 6)" -v z="$$(tput sgr0)" '{printf"- %s%s%s\n",a,$$1,z;m=split($$2,w,"---");l=n-i;for(j=1;j<=m;j++){l-=length(w[j])+1;if(l<= 0){l=n-i-length(w[j])-1;}printf"%*s%s\n",-i," ",w[j];}}'
