# Overview

## Objective:
Analyze gene expression data generated from blood samples from Alzheimer’s Disease (AD) and mild cognitive impairment (MCI) patients, and non-pathologic control subjects, and develop classification models (at least for AD vs. control, not sure if there is sufficient data to be able to include MCI outcome). For example, if you were to analyze genome-wide expression data and find gene expression with non-zero coefficient estimates from Lasso/Elastic Net or SHAP scores from XGBoost, you could then potentially develop a custom chip that selectively measures those genes, which you could use in patients as a novel diagnostic tool.

## Significance:
Currently, AD diagnosis involves multiple visits to specialists and performing various clinical assessments, and the fact that there are various other neurological diseases that could manifest similarly to AD makes clinical diagnosis arduous & challenging. Therefore, developing a quick blood test allowing AD classification is expected to shorten the timeline of diagnosis, and improve monitoring & early diagnosis in the primary care setting, as well as make it possible to develop a commercial at-home kit. Facilitating diagnosis is also expected to reduce healthcare costs, highlighting benefits to both individual patients and societally.

## Advantage of using blood data:
The source of blood can be from routine blood draws in the primary care setting, so it is suitable for the purpose of developing a non-invasive diagnostic tool that can be easily implemented in the clinical setting. Also, it is easy to find free data because it is a common source of tissue for generating gene expression data in published papers, making it possible to have sufficient data points to develop prediction models and externally validate them.

## Common blood samples used:
Peripheral whole blood (all blood cells) or peripheral whole blood or peripheral blood mononuclear cells (PBMCs) (mostly white blood cells) are commonly used. Baseline gene expression patterns would be different for whole blood vs. PBMCs, so studies tend to analyze them separately.

## Disadvantage of using blood data:
The results may or may not provide much mechanistic understanding because you are not investigating gene expression associated with AD pathology in the brain, where the condition actually occurs. However, given the objective of developing a diagnostic tool, it is better to use data derived from blood, rather than postmortem brain samples. (On the other hand, if you wanted to develop therapeutics, you would want to look at the brain itself.)

## National Center for Biotechnology Information Gene Expression Omnibus (NCBI GEO):
A public repository where you can find a lot of gene expression datasets. You neither have to apply for Institutional Review Board (IRB) clearance nor sign a Data User Agreement, which makes gene expression data highly accessible. It is one of NIH’s efforts for increasing opportunities for secondary analyses of data generated from previous grant-funded studies.

## Methods for generating gene expression data & notes:
The NCBI GEO data is mainly generated by the ‘microarray’ assay, where there is a chip with many probes, each corresponding to a specific gene. If a gene is expressed, the probe turns color - then, that is converted to numeric values. Note that you need to worry about batch effects between studies (so it is common to do PCA to first visualize possible problems with batch effect, and implement correction methods if so… but what is uploaded in the repository should have taken care of this within each dataset); also, different companies produce different chips/platforms, so there could be some genes that are measured in one study, but not in another, depending on the chip/platform used for the specific study.

# Datasets

## Dataset 1:
File names: GSE140829_final_normalized_data.txt = the gene expression data & GSE140829_datalabels.xlsx = information on the samples
Notes: “GENE_SYMBOL” column = name of each gene whose expression level is measured. Note: There seem to be various technical replicates from the same patient, I need to read the associated paper more carefully and check.
Samples: Whole blood (207 labeled as Control; 182 labeled as AD; 111 labeled as Mild Cognitive Impairment (MCI))
Source: https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE140829
Associated publication: https://www.biorxiv.org/content/10.1101/2019.12.13.875112v1.full



## Dataset 2:
File names: GSE85426_normalized_data_m3_use.txt = the gene expression data & GSE85426_datalabels.xlsx = information on the samples, but I still need to look for the information on the column name and how that relates to the sample label…
Notes: “GENE_SYMBOL” column = name of each gene whose expression level is measured. Note: there are 8543 genes that are overlapping between Dataset 1 & Dataset 2 because they were using different platforms, and Dataset 2 is log2-transformed (which is common to do for microarray data), but Dataset 1 was not.
Samples: Whole blood (90 Controls & 90 AD)
Source: https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE85426


## Dataset 3:
File names: GSE18309_RMAnorm_2.txt =  the gene expression data & GSE18309_datalabels.xlsx = information on the samples
Notes: They only had the chip .CEL files available, so I downloaded those and applied ‘RMA’ which is one of the most common methods for converting .CEL files to numeric values. Dataset 3 is log2-transformed because RMA does that automatically.
Source: https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE18309
Samples: PBMCs (3 Controls, 3 AD, 3 MCI) - note this is not whole blood, but you might be able to try to see if this can be used for validation.


## Dataset 4:
File names: Still need to obtain.
Source: https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE4229
Associated publication: https://academic.oup.com/biomedgerontology/article/64A/6/636/625915?login=false
Samples: PBMC (Normal elderly control (NEC) and Alzheimer disease (AD) subjects. Targets from biological replicates of NEC (n=22) and AD (n=18))


## Dataset 5:
File names: Still need to obtain.
Source: https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE4226
Associated publication: https://pubmed.ncbi.nlm.nih.gov/16979800/
Samples: Control Alzheimer disease (AD) subjects. Targets from biological replicates of female (n=7) and male (n=7) NEC and female (n=7) and male (n=7) AD


## Dataset 6:
File names: Still need to obtain.
Source: https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE97760
Associated publication: https://pubmed.ncbi.nlm.nih.gov/25079797/
Samples: Whole blood (9 female healthy controls; 9 female AD)
