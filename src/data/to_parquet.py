from pathlib import Path

import numpy as np
import pandas as pd
import typer

app = typer.Typer()


@app.command()
def convert_dataset_1(
    data_file: Path = typer.Option(...),
    label_file: Path = typer.Option(...),
    output_file: Path = typer.Option(...),
) -> None:
    """
    File names:
        GSE140829_final_normalized_data.txt = the gene expression data
        GSE140829_datalabels.xlsx = information on the samples
    Notes:
        “GENE_SYMBOL” column = name of each gene whose expression level is measured.
        There seem to be various technical replicates from the same patient, I
        need to read the associated paper more carefully and check.
        [MATT]: This is NOT log2-transformed
    Samples:
        Whole blood
        207 labeled as Control
        182 labeled as Alzheimer’s Disease (AD)
        111 labeled as Mild Cognitive Impairment (MCI)
    Source: https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE140829
    Associated publication: https://www.biorxiv.org/content/10.1101/2019.12.13.875112v1.full
    """

    # data_df:
    #       GENE_SYMBOL  5872617031_A  5872617031_C  5872617031_E  5872641011_B  ...
    # 0            A1BG      7.394524      7.495104      7.372404      7.521084  ...
    # 1        A1BG-AS1      7.282190      7.286154      7.297627      7.302201  ...
    # 2            A1CF      7.349359      7.509113      7.288939      7.372468  ...
    # 3         A3GALT2      7.275445      7.252396      7.284068      7.324256  ...
    # 4          A4GALT      7.254837      7.243199      7.219352      7.272022  ...
    # ...
    data_df = pd.read_csv(data_file, sep="\t")
    data_df["GENE_SYMBOL"] = data_df.GENE_SYMBOL.apply(
        lambda symbol: f"GENE_SYMBOL_{symbol}"
    )
    # apply the log2 transformation.
    # this doesn't seem accurate as the range of the resulting values does not match dataset 2
    for column in data_df.columns[1:]:
        data_df[column] = np.log2(data_df[column])
    data_df = data_df.set_index("GENE_SYMBOL")

    # label_df:
    #       Sample ID       Tissue  Outcome     Column name
    # 0    GSM4187187  Whole blood  Control  200363680054_G
    # 1    GSM4187188  Whole blood      MCI    5786049087_G
    # 2    GSM4187189  Whole blood  Control    5937345007_F
    # 3    GSM4187190  Whole blood      MCI  200363680054_L
    # 4    GSM4187191  Whole blood  Control    5872641012_H
    # ...
    # 500 rows
    # Tissue: Whole blood 500
    # Outcome: Control 207, AD 182, MCI 111
    label_df = pd.read_excel(label_file)

    # outcome has leading spaces, column name has leading and trailing spaces
    label_df["Outcome"] = label_df.Outcome.str.strip()
    label_df["Column name"] = label_df["Column name"].str.strip()

    label_df = label_df.rename(
        columns={
            "Sample ID": "sample_id",
            "Tissue": "tissue",
            "Outcome": "outcome",
            "Column name": "column",
        }
    )
    label_df = label_df.set_index("column")

    df = label_df.merge(data_df.T, left_index=True, right_index=True)
    df.to_parquet(output_file, compression="gzip")
    print(f"written {len(df):,} labelled rows to {output_file}")


@app.command()
def convert_dataset_2(
    data_file: Path = typer.Option(...),
    label_file: Path = typer.Option(...),
    matrix_file: Path = typer.Option(...),
    output_file: Path = typer.Option(...),
) -> None:
    """
    File names:
        GSE85426_normalized_data_m3_use.txt = the gene expression data
        GSE85426_datalabels.xlsx = information on the samples
        I still need to look for the information on the column name and how
        that relates to the sample label…
    Notes:
        “GENE_SYMBOL” column = name of each gene whose expression level is measured.
        There are 8543 genes that are overlapping between Dataset 1 & Dataset 2
        because they were using different platforms, and Dataset 2 is
        log2-transformed (which is common to do for microarray data), but
        Dataset 1 was not.
    Samples: Whole blood (90 Controls & 90 AD)
    Source: https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE85426
    """

    # data df:
    #                Probe   GENE_SYMBOL  ...  US81403231_252800416965_S01_GE1_107_Sep09_2_4.txt
    # 0       A_23_P100074          AVEN  ...                                           0.196364
    # 1       A_23_P100141          UNKL  ...                                           1.289359
    # 2       A_23_P100196         USP10  ...                                          -0.572497
    # 3       A_23_P100203         HSBP1  ...                                           0.305984
    # 4       A_23_P100220         ESRP2  ...                                          -0.042503
    # column names very strange
    # 10563 rows, 182 columns
    # so columns relate to the label df, need a way to correlate them
    # The column name is made up of 8 sections separated by underscores
    # The distribution of values is:
    # 0: US81403231 172, US91203660 8
    # 1: 252800416965 ... (29 distinct values with counts ranging from 8 to 1)
    # 2: S01 180
    # 3: GE1 180
    # 4: 107 180
    # 5: Sep09 180
    # 6: 1 91, 2 89
    # 7: 1 43, 2 44, 3 47, 4 46
    # It is not clear how the Sample ID correlates to the column name

    # I've downloaded GSE85426_normalized_data.txt.gz and GSE85426_raw_data.txt.gz from the website.
    # They are completely unprocessed.
    # I've downloaded GSE85426_series_matrix.txt.gz and there is a correlation
    # between the GSM2266789 etc values and the column names.
    matrix_df = pd.read_csv(matrix_file, sep="\t", skiprows=28)
    id_to_column = {
        sample_id: f"{column}.txt"
        for sample_id, column in matrix_df.iloc[20][1:].to_dict().items()
    }

    data_df = pd.read_csv(data_file, sep="\t")
    data_df["GENE_SYMBOL"] = data_df.GENE_SYMBOL.apply(
        lambda symbol: f"GENE_SYMBOL_{symbol}"
    )
    # there are 10563 rows and 10494 distinct row names, dropping duplicates arbitrarily
    old_len = len(data_df)
    data_df = data_df.drop_duplicates(subset="GENE_SYMBOL", keep="first")
    print(f"dropped {old_len - len(data_df):,} genes")
    data_df = data_df.set_index("GENE_SYMBOL")
    data_df = data_df.drop(columns="Probe")

    # label df:
    #       Sample ID       Tissue                  Outcome  Column name
    # 0    GSM2266610  Whole blood   Non-demented control 1          NaN
    # 1    GSM2266611  Whole blood            Probable AD 1          NaN
    # 2    GSM2266612  Whole blood            Probable AD 2          NaN
    # 3    GSM2266613  Whole blood   Non-demented control 2          NaN
    # 4    GSM2266614  Whole blood   Non-demented control 3          NaN
    # 180 rows
    # Tissue: Whole blood 180
    # Outcome: Each outcome has a numeric suffix which makes them unique
    #   without that, Non-demented control 90, Probable AD 90
    # No Column name is set
    label_df = pd.read_excel(label_file)

    label_df["Outcome"] = label_df.Outcome.apply(
        lambda outcome: " ".join(outcome.split(" ")[:-1])
    )
    label_df["Outcome"] = label_df.Outcome.map(
        {"Non-demented control": "Control", "Probable AD": "AD"}
    )
    label_df["Column name"] = label_df["Sample ID"].map(id_to_column)

    label_df = label_df.rename(
        columns={
            "Sample ID": "sample_id",
            "Tissue": "tissue",
            "Outcome": "outcome",
            "Column name": "column",
        }
    )
    label_df = label_df.set_index("column")

    df = label_df.merge(data_df.T, left_index=True, right_index=True)
    df.to_parquet(output_file, compression="gzip")
    print(f"written {len(df):,} labelled rows to {output_file}")


@app.command()
def convert_dataset_3(
    data_file: Path = typer.Option(...),
    label_file: Path = typer.Option(...),
    output_file: Path = typer.Option(...),
) -> None:
    """
    File names:
        GSE18309_RMAnorm_2.txt = the gene expression data
        GSE18309_datalabels.xlsx = information on the samples
    Notes:
        They only had the chip .CEL files available, so I downloaded those and
        applied ‘RMA’ which is one of the most common methods for converting
        .CEL files to numeric values. Dataset 3 is log2-transformed because RMA
        does that automatically.
    Samples:
        PBMCs (3 Controls, 3 AD, 3 MCI) - note this is not whole blood, but you
        might be able to try to see if this can be used for validation.
    Source: https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE18309
    """

    # data df:
    #       GENE_SYMBOL  GSM457169.CEL  GSM457170.CEL  GSM457171.CEL  GSM457172.CEL
    # 0            DDR1       5.641936       5.856834       5.629645       5.743109
    # 1            RFC2       6.060132       6.151508       6.632557       6.469616
    # 2           HSPA6       8.995219       9.639991      10.318151      10.005083
    # 3            PAX8       6.803412       7.008647       6.986813       6.930690
    # 4          GUCA1A       2.832221       2.747173       2.637015       2.712485
    # 43091 rows 10 columns
    data_df = pd.read_csv(data_file, sep="\t")
    data_df["GENE_SYMBOL"] = data_df.GENE_SYMBOL.apply(
        lambda symbol: f"GENE_SYMBOL_{symbol}"
    )
    data_df = data_df.rename(
        columns={
            column: column.split(".")[0]
            for column in data_df.columns
            if column.endswith(".CEL")
        }
    )
    # there are 43091 rows and 20824 distinct row names, dropping duplicates arbitrarily
    # this is a difference of 22267 rows!
    old_len = len(data_df)
    data_df = data_df.drop_duplicates(subset="GENE_SYMBOL", keep="first")
    print(f"dropped {old_len - len(data_df):,} genes")
    data_df = data_df.set_index("GENE_SYMBOL")

    # label df:
    #    Sample ID Tissue                   Outcome Column name
    # 0  GSM457169  PBMCs              AD patient 1   GSM457169
    # 1  GSM457170  PBMCs              AD patient 2   GSM457170
    # 2  GSM457171  PBMCs              AD patient 3   GSM457171
    # 3  GSM457172  PBMCs             MCI patient 1   GSM457172
    # 4  GSM457173  PBMCs             MCI patient 2   GSM457173
    # 5  GSM457174  PBMCs             MCI patient 3   GSM457174
    # 6  GSM457175  PBMCs  Normal elderly control 1   GSM457175
    # 7  GSM457176  PBMCs  Normal elderly control 2   GSM457176
    # 8  GSM457177  PBMCs  Normal elderly control 3   GSM457177
    # This is the COMPLETE dataframe
    label_df = pd.read_excel(label_file)

    # outcome has leading spaces, column name has leading and trailing spaces
    label_df["Outcome"] = label_df.Outcome.apply(
        lambda outcome: " ".join(outcome.split(" ")[:-1])
    )
    label_df["Outcome"] = label_df.Outcome.map(
        {"AD patient": "AD", "MCI patient": "MCI", "Normal elderly control": "Control"}
    )

    label_df = label_df.rename(
        columns={
            "Sample ID": "sample_id",
            "Tissue": "tissue",
            "Outcome": "outcome",
            "Column name": "column",
        }
    )
    label_df = label_df.set_index("column")

    df = label_df.merge(data_df.T, left_index=True, right_index=True)
    df.to_parquet(output_file, compression="gzip")
    print(f"written {len(df):,} labelled rows to {output_file}")


@app.command()
def convert_dataset_4(
    data_file_1: Path = typer.Option(...),
    data_file_2: Path = typer.Option(...),
    data_file_3: Path = typer.Option(...),
    data_file_4: Path = typer.Option(...),
    output_file: Path = typer.Option(...),
) -> None:
    """
    File names:
        AD_plasma_protein.xlsx
        AD_plasma_protein_MCI.xlsx
        AD_plasma_protein_test.xlsx
        AD_plasma_protein_testOD.xlsx
    Notes:
        This dataset was provided without any knowledge of the source.
        There is an example of using it to produce an xgboost model in src/R/PlasmaProtein_Study_5.R
        The first thing that script does is merge the datasets, which I am replicating here.
        The columns have a prefix which seems to correlate with outcome:
            AD          85
            NDC         79
            MCI-AD      22
            MCI-MCI     17
            OD          11
            VaD          4
            LewyBody     3
            FTD          1
    Source: UNKNOWN
    """
    # The actual data processing R code:
    # AD_plasma_protein <- read_excel("../../data/external/AD_plasma_protein.xlsx")
    # AD_plasma_protein <- as.data.frame(AD_plasma_protein)
    # row.names(AD_plasma_protein) <- AD_plasma_protein$CLASS
    # AD_plasma_protein <- AD_plasma_protein[,names(AD_plasma_protein) %nin% "CLASS"]
    #
    # AD_plasma_protein_test <- read_excel("../../data/external/AD_plasma_protein_test.xlsx")
    # AD_plasma_protein_test  <- as.data.frame(AD_plasma_protein_test)
    # row.names(AD_plasma_protein_test) <- AD_plasma_protein_test$CLASS
    # AD_plasma_protein_test <- AD_plasma_protein_test[, names(AD_plasma_protein_test)  %nin% "CLASS"]
    #
    # AD_plasma_protein_testOD <- read_excel("../../data/external/AD_plasma_protein_testOD.xlsx")
    # AD_plasma_protein_testOD <- as.data.frame(AD_plasma_protein_testOD)
    # row.names(AD_plasma_protein_testOD) <- AD_plasma_protein_testOD$CLASS
    # AD_plasma_protein_testOD <- AD_plasma_protein_testOD[, names(AD_plasma_protein_testOD) %nin% "CLASS"]
    #
    # AD_plasma_protein_MCI <- read_excel("../../data/external/AD_plasma_protein_MCI.xlsx")
    # AD_plasma_protein_MCI <- as.data.frame(AD_plasma_protein_MCI)
    # row.names(AD_plasma_protein_MCI) <- AD_plasma_protein_MCI$CLASS
    # AD_plasma_protein_MCI <- AD_plasma_protein_MCI[,names(AD_plasma_protein_MCI) %nin% "CLASS"]
    #
    # It's read, converted to a dataframe, and then the CLASS column is used as the index.

    def load(file: Path) -> pd.DataFrame:
        df = pd.read_excel(file)
        df = df.set_index("CLASS")
        return df

    df_1 = load(data_file_1)
    df_2 = load(data_file_2)
    df_3 = load(data_file_3)
    df_4 = load(data_file_4)

    # After this the frames are merged:
    # merged.dat <- merge(AD_plasma_protein, AD_plasma_protein_test, by="row.names")
    # row.names(merged.dat) <- merged.dat$Row.names
    # merged.dat <- merged.dat[,names(merged.dat) %nin% "Row.names"]
    #
    # merged.dat.2 <- merge(AD_plasma_protein_MCI, AD_plasma_protein_testOD, by="row.names")
    # row.names(merged.dat.2) <- merged.dat.2$Row.names
    # merged.dat.2 <- merged.dat.2[,names(merged.dat.2) %nin% "Row.names"]
    #
    # merged.dat.3 <- merge(merged.dat, merged.dat.2, by="row.names")
    # row.names(merged.dat.3) <- merged.dat.3$Row.names
    # merged.dat.3 <- merged.dat.3[,names(merged.dat.3) %nin% "Row.names"]
    #
    # We could merge in the same way, if we transpose the frames then they can be concatenated.
    # This would also be more consistent with the other dataset preprocessing.

    df = pd.concat(
        [
            df_1.T,
            df_2.T,
            df_3.T,
            df_4.T,
        ]
    )

    columns = list(df.columns)
    df["outcome"] = df.index
    df["outcome"] = df.outcome.str.split("_")
    df["outcome"] = df.outcome.apply(lambda outcome: outcome[0])

    df = df[["outcome"] + columns]

    df.to_parquet(output_file, compression="gzip")


if __name__ == "__main__":
    app()
